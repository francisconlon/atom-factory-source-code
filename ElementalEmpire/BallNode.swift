//
//  BallNode.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 12/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import SpriteKit

class BallNode: SKShapeNode
{
    var special: Bool
    
    
    init(special: Bool, radius: CGFloat)
    {
        self.special = special
        
        super.init()
        
        let shell = SKSpriteNode(texture: SKTexture(imageNamed: "hexagon.png"),
                                 color: UIColor.orange,
                                 size: CGSize(width: radius * 2,
                                              height: radius * 2))
        addChild(shell)
        shell.colorBlendFactor = 1
        if special
        {
            shell.color = SKColor.cyan
            
        } else {
            //shell.fillColor = UIColor.orange
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}
