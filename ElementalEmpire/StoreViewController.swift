//
//  StoreViewController.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 29/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import StoreKit
import GoogleMobileAds
import BigInt
import PersonalizedAdConsent

class StoreViewController: UIViewController
{
    
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var contentView: UIView!
    
    @IBOutlet var freeSpinner: UIActivityIndicatorView!
    
    @IBOutlet var resetButton: UIButton!
    @IBOutlet var advertInfoButton: UIButton!
    
    @IBOutlet var advertButton: UIButton!
    @IBOutlet var smallQuantityButton: UIButton!
    @IBOutlet var mediumQuantityButton: UIButton!
    @IBOutlet var largeQuantityButton: UIButton!
    
    let freeQuantity = 20
    let smallQuantity = 200
    let mediumQuantity = 500
    let largeQuantity = 1000
    
    @IBOutlet var smallPriceLabel: UILabel!
    @IBOutlet var mediumPriceLabel: UILabel!
    @IBOutlet var largePriceLabel: UILabel!
    
    @IBOutlet var resetButtonLabel: UILabel!
    @IBOutlet var buttonView: UIView!
    
    @IBOutlet var advertTopConstraint: NSLayoutConstraint!
    @IBOutlet var smallTopConstraint: NSLayoutConstraint!
    @IBOutlet var mediumTopConstraint: NSLayoutConstraint!
    @IBOutlet var largeTopConstraint: NSLayoutConstraint!
    
    var smallID = "com.overvoltstudios.elementalempire.smallquantity"
    var mediumID = "com.overvoltstudios.elementalempire.mediumquantity"
    var largeID = "com.overvoltstudios.elementalempire.largequantity"
    var productIDSet = Set<String>()
    
    var smallProduct: SKProduct!
    var mediumProduct: SKProduct!
    var largeProduct: SKProduct!
    
    var rewardBool = false

    
    override func viewDidLoad()
    {        
        super.viewDidLoad()

        doneButton.layer.cornerRadius = 5
        contentView.layer.cornerRadius = contentView.frame.size.width * 0.05
        // Do any additional setup after loading the view.
        
        smallPriceLabel.adjustsFontSizeToFitWidth = true
        
        mediumPriceLabel.adjustsFontSizeToFitWidth = true

        largePriceLabel.adjustsFontSizeToFitWidth = true

        
        resetButtonLabel.adjustsFontSizeToFitWidth = true
        resetButtonLabel.text = String("Reset for \(ProgressSave.shared.currentElement - 10)")
        
        if ProgressSave.shared.currentElement <= 10
        {
            resetButton.removeFromSuperview()
            resetButtonLabel.removeFromSuperview()
        }
        
        productIDSet.insert(smallID)
        productIDSet.insert(mediumID)
        productIDSet.insert(largeID)
        
        
        if (SKPaymentQueue.canMakePayments())
        {
            var productsRequest = SKProductsRequest(productIdentifiers: productIDSet)
            productsRequest.delegate = self
            productsRequest.start()
            SKPaymentQueue.default().add(self)
        } else {
            
        }
        
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        
        if GADRewardBasedVideoAd.sharedInstance().isReady
        {
            freeSpinner.alpha = 0
        }
        
        if PACConsentInformation.sharedInstance.isRequestLocationInEEAOrUnknown
        {
            advertInfoButton.alpha = 1
        } else {
            advertInfoButton.removeFromSuperview()
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        let gap = buttonView.frame.size.height - (advertButton.frame.size.height * 4)
        let spacing = gap / 5
        
        advertTopConstraint.constant = spacing
        smallTopConstraint.constant = spacing
        mediumTopConstraint.constant = spacing
        largeTopConstraint.constant = spacing
        buttonView.layoutIfNeeded()
        
        
        
        smallPriceLabel.layoutIfNeeded()
        mediumPriceLabel.layoutIfNeeded()
        largePriceLabel.layoutIfNeeded()
    }
    
    func buyProduct(product: SKProduct)
    {
        var payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    @IBAction func resetButtonPressed(_ sender: UIButton)
    {
        Factory.shared.animateUIButton(button: sender)
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5)
        {
            let uvc = self.presentingViewController as! UpgradesViewController
            
            self.dismiss(animated: true, completion: {
                uvc.resetGame()
            })
            
            
        }
        
        ProgressSave.shared.specialEnergy += BigInt(ProgressSave.shared.currentElement - 10)
        ProgressSave.shared.saveChanges()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + Factory.shared.duration)
        {
            
            let uvc = self.presentingViewController as! UpgradesViewController
            uvc.updatePurchase()
        }
        
        
    }
    
    @IBAction func doneButtonPressed(_ sender: UIButton)
    {
        Factory.shared.animateUIButton(button: sender)
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)

        let uvc = self.presentingViewController as! UpgradesViewController
        UIView.animate(withDuration: 0.2, animations: {
            uvc.doneButton.alpha = 1
        }) { (done) in
            uvc.doneButton.isEnabled = true
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + Factory.shared.duration) {
            self.dismiss(animated: true, completion: nil)
        }
        
        ProgressSave.shared.saveChanges()

    }
    
    @IBAction func smallButtonPressed(_ sender: UIButton)
    {
        if smallProduct != nil
        {
            Factory.shared.animateUIButton(button: sender)
            buyProduct(product: smallProduct)
            SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)

        } else {
            SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonInvalidSound)
        }
    }
    
    @IBAction func mediumButtonPressed(_ sender: UIButton)
    {
        if mediumProduct != nil
        {
            Factory.shared.animateUIButton(button: sender)
            buyProduct(product: mediumProduct)
            SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)

        } else {
            SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonInvalidSound)
        }
    }
    
    @IBAction func largeButtonPressed(_ sender: UIButton)
    {
        if largeProduct != nil
        {
            Factory.shared.animateUIButton(button: sender)
            buyProduct(product: largeProduct)
            SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)

        } else {
            SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonInvalidSound)
        }
    }
    
    @IBAction func freeButtonPressed(_ sender: UIButton)
    {
        Factory.shared.animateUIButton(button: sender)
        
        if GADRewardBasedVideoAd.sharedInstance().isReady
        {
            if let bp = SKTAudio.sharedInstance().backgroundMusicPlayer { bp.pause() }
            GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
            freeSpinner.alpha = 1
        } else {
            SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonInvalidSound)
        }
    }
}

extension StoreViewController: SKProductsRequestDelegate
{
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
    {
        for product in response.products
        {
            switch product.productIdentifier
            {
            case smallID:
                smallProduct = product
                var string = String()
                if let symbol = product.priceLocale.currencySymbol
                {
                    string.append(symbol)
                }
                string.append(product.price.stringValue)
                smallPriceLabel.text = string
            case mediumID:

                mediumProduct = product
                
                var string = String()
                if let symbol = product.priceLocale.currencySymbol
                {
                    string.append(symbol)
                }
                string.append(product.price.stringValue)
                mediumPriceLabel.text = string
            case largeID:

                largeProduct = product
                
                var string = String()
                if let symbol = product.priceLocale.currencySymbol
                {
                    string.append(symbol)
                }
                string.append(product.price.stringValue)
                largePriceLabel.text = string
            default:
                break
            }
        }
    }
    
    
}
extension StoreViewController: SKPaymentTransactionObserver
{
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions
        {
            switch transaction.transactionState {
            case .purchased:
                
                switch transaction.payment.productIdentifier {
                case smallID:
                    ProgressSave.shared.specialEnergy += BigInt(smallQuantity)
                case mediumID:
                    ProgressSave.shared.specialEnergy += BigInt(mediumQuantity)
                case largeID:
                    ProgressSave.shared.specialEnergy += BigInt(largeQuantity)
                default:
                    break
                }
                
                ProgressSave.shared.saveChanges()
                let uvc = self.presentingViewController as! UpgradesViewController
                uvc.updatePurchase()
                

                SKPaymentQueue.default().finishTransaction(transaction)
                break;
            case .failed:
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            default:
                break
            }
        }
    }
}

extension StoreViewController: GADRewardBasedVideoAdDelegate
{
    
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd)
    {
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(), withAdUnitID: Factory.shared.freeAdID)
        
        if let bp = SKTAudio.sharedInstance().backgroundMusicPlayer { bp.play() }
        
        if rewardBool
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1)
            {
                let uvc = self.presentingViewController as! UpgradesViewController
                uvc.updatePurchase()
                self.rewardBool = false
            }
            
        }
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didRewardUserWith reward: GADAdReward) {
        ProgressSave.shared.specialEnergy += BigInt(freeQuantity)
        ProgressSave.shared.saveChanges()
        
        rewardBool = true
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        freeSpinner.alpha = 0
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didFailToLoadWithError error: Error) {
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(), withAdUnitID: Factory.shared.freeAdID)
    }
}
