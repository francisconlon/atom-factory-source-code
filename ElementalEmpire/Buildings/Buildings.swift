//
//  Elements.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 06/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import BigInt

class Buildings
{
    static let shared = Buildings()
    
    var buildings: [Building]
    
    private init()
    {
        buildings = [Building]()
        let path = Bundle.main.url(forResource: "Buildings", withExtension: ".plist")
        let buildingsArray = NSArray(contentsOfFile: path!.path) as! [NSDictionary]
        for i in 0..<buildingsArray.count
        {
            let buildingDict = buildingsArray[i]
            let nameA = buildingDict["Name"] as! String
            let priceA = buildingDict["InitialPrice"] as! String
            let powerA = buildingDict["Power"] as! String
            let tier = buildingDict["Tier"] as! Int
            
            let priceB = BigInt(priceA)!
            let powerB = BigInt(powerA)!
            
            let building = Building(name: nameA, price: priceB, power: powerB, type: tier)
            building.price = building.priceForQuantity(quantity: ProgressSave.shared.numberOwned[i])
            buildings.append(building)
        }
    }
    
    func reset()
    {
        buildings = [Building]()
        let path = Bundle.main.url(forResource: "Buildings", withExtension: ".plist")
        let buildingsArray = NSArray(contentsOfFile: path!.path) as! [NSDictionary]
        for i in 0..<buildingsArray.count
        {
            let buildingDict = buildingsArray[i]
            let nameA = buildingDict["Name"] as! String
            let priceA = buildingDict["InitialPrice"] as! String
            let powerA = buildingDict["Power"] as! String
            let type = buildingDict["Tier"] as! Int
            
            let priceB = BigInt(priceA)!
            let powerB = BigInt(powerA)!
            
            let building = Building(name: nameA, price: priceB, power: powerB, type: type)
            building.price = building.priceForQuantity(quantity: ProgressSave.shared.numberOwned[i])
            buildings.append(building)
        }
    }
    
}
