//
//  Element.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 06/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import BigInt

class Building: NSObject
{
    var name: String
    var price: BigInt
    var power: BigInt
    var type: Int
    
    var priceIncrease:BigInt = 115
    
    init(name: String, price: BigInt, power: BigInt, type: Int)
    {
        self.name = name
        self.price = price
        self.power = power
        self.type = type
        super.init()
    }
    
    func increasePriceFor(count: BigInt)
    {
        //if count > 2
        //{
            for i in 1...(count)
            {
                price = (price * priceIncrease)/100
            }
        //}
        
    }
    
    func priceForQuantity(quantity: BigInt) -> BigInt
    {
        var newPrice = BigInt(price)
        if quantity > 2
        {
            var x = 0
            var currentPrice = BigInt(price)
            for i in 1...(quantity - 1)
            {
                currentPrice = (currentPrice * priceIncrease) / 100
                newPrice += currentPrice
                

                x += 1
                
            }
        }
        
        
        
        return newPrice
    }
    
}

