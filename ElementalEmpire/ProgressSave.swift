//
//  Elements.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 06/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import BigInt

class ProgressSave
{
    static let shared = ProgressSave()
    
    var numberOwned: [BigInt]
    var energyLabel: UILabel!
    var powerLabel: UILabel!
    var gameScene: GameScene!
    var timer: Timer!
    
    var clickMultiplierIndex: Int {
        didSet {
            clickMultiplier = clickMultiplierIndex
        }
    }
    var buildingsMultiplierIndex: [Int] {
        didSet {
        }
    }
    
    var clickMultiplier: Int!
    
    var saveURL: URL!
    var fileURL: URL!
    var fm: FileManager!
    
    var lastDateClosed: NSDate
    var unlockedBuildings: Int
    var target: BigInt
    var currentElement: Int
    var selectedQuantity: Int
    var specialEnergy: BigInt
    
    var energy: BigInt {
        didSet {
            if energyLabel != nil
            {
                energyLabel.text = String("\(formatBigInt(bigInt: energy))J")
            }
        }
    }
    var power: BigInt
    {
        didSet
        {
            if powerLabel != nil
            {
                powerLabel.text = String("\(formatBigInt(bigInt: power))W")
            }
        }
    }
    
    func reset()
    {
        self.power = BigInt(0)
        self.energy = BigInt(0)
        self.unlockedBuildings = 1
        self.numberOwned = [BigInt](repeating: 0, count: 50)
        self.target = 10
        self.currentElement = 0
        
        self.saveChanges()
        
        Buildings.shared.reset()
    }
    
    func updatePowerLabel()
    {
        if powerLabel != nil
        {
            powerLabel.text = String("\(formatBigInt(bigInt: power))W")
        }
    }
    
    func updateEnergyLabel()
    {
        if energyLabel != nil
        {
            energyLabel.text = String("\(formatBigInt(bigInt: energy))J")
        }
    }
    
    func formatBigInt(bigInt: BigInt) -> String
    {
        let string = String(bigInt)
        if string.count >= 1
        {
            var i = (string.count - 1)/3
            if i >= 1
            {
                var divisionFactor = BigInt(1000)
                if i > 1
                {
                    for x in 1..<i
                    {
                        divisionFactor *= 1000
                    }
                }
                
                let displayEnergy = bigInt.quotientAndRemainder(dividingBy: divisionFactor).quotient
                let displayRemainder = bigInt.quotientAndRemainder(dividingBy: divisionFactor).remainder
                
                var a = String(divisionFactor).count - 1
                var string = String(displayRemainder)
                while string.count != a
                {
                    string.insert("0", at: string.startIndex)
                }
                while string.count != 3
                {
                    string.removeLast()
                }
                var prefix = String()
                while i - 1 >= 8
                {
                    prefix.append(contentsOf: String(describing: Prefixes(rawValue: 7)!))
                    i -= 8
                }
                
                
                prefix.append(contentsOf: String(describing: Prefixes(rawValue: i - 1)!))
                
                var displayString = String(displayEnergy)
                while displayString.count > 4
                {
                    displayString.popLast()
                }
                
                let finalString = String("\(displayString).\(string)\(prefix)")
                return finalString
            } else {
                return String("\(bigInt)")
            }
        } else {
            return "0"
        }
    }
    
    private init()
    {
        currentElement = 0
        energy = 0
        power = 0
        target = 10
        specialEnergy = 20
        unlockedBuildings = 1
        numberOwned = [BigInt](repeating: 0, count: 50)
        lastDateClosed = NSDate()
        clickMultiplierIndex = 1
        buildingsMultiplierIndex = [Int](repeating: 1, count: 50)
        selectedQuantity = 1
                
        fm = FileManager.default
        guard let directory = fm.urls(for: .libraryDirectory, in: .userDomainMask).first else { return }
        
        saveURL = directory.appendingPathComponent("Saves")
        fileURL = saveURL.appendingPathComponent("Save")
        
        if FileManager.default.fileExists(atPath: fileURL.path)
        {
            let data = NSMutableDictionary(contentsOfFile: fileURL.path)!
            let energyString = data["Energy"] as! String
            let powerString = data["Power"] as! String
            let targetString = data["Target"] as! String
            let specialEnergyString = data["SpecialEnergy"] as! String
            let numberOwnedString = data["NumberOwned"] as! [String]
            let curS = data["CurrentElement"] as! Int
            let ldc = data["lastDateClosed"] as! Double
            let unlockBuildings = data["unlockedBuildings"] as! Int
            let buildingsMultiInd = data["buildingsMultiplier"] as! [Int]
            let clickMultiInd = data["clickMultiplier"] as! Int
            let selQuant = data["SelectedQuantity"] as! Int
            
            currentElement = curS
            specialEnergy = BigInt(specialEnergyString)!
            energy = BigInt(energyString)!
            power = BigInt(powerString)!
            target = BigInt(targetString)!
            numberOwned = [BigInt]()
            lastDateClosed = NSDate.init(timeIntervalSinceReferenceDate: ldc)
            unlockedBuildings = unlockBuildings
            clickMultiplierIndex = clickMultiInd
            buildingsMultiplierIndex = buildingsMultiInd
            selectedQuantity = selQuant
            for i in numberOwnedString
            {
                let s = BigInt(i)!
                numberOwned.append(s)
            }
            
        } else {
            do { try fm.createDirectory(at: saveURL, withIntermediateDirectories: true, attributes: nil)} catch let error as NSError{print(error)}
            
        }
    }
    
    func saveChanges()
    {
        let data = NSMutableDictionary()
        
        let eneS = String(energy)
        let powS = String(power)
        let speS = String(specialEnergy)
        var numS = [String]()
        for i in numberOwned
        {
            let s = String(i)
            numS.append(s)
        }
        let tarS = String(target)
        
        lastDateClosed = NSDate.init(timeIntervalSinceReferenceDate: NSDate().timeIntervalSinceReferenceDate)
        
        
        data.setValue(eneS, forKey: "Energy")
        data.setValue(speS, forKey: "SpecialEnergy")
        data.setValue(powS, forKey: "Power")
        data.setValue(tarS, forKey: "Target")
        data.setValue(numS, forKey: "NumberOwned")
        data.setValue(currentElement, forKey: "CurrentElement")
        data.setValue(NSDate().timeIntervalSinceReferenceDate, forKey: "lastDateClosed")
        data.setValue(unlockedBuildings, forKey: "unlockedBuildings")
        data.setValue(clickMultiplierIndex, forKey: "clickMultiplier")
        data.setValue(buildingsMultiplierIndex, forKey: "buildingsMultiplier")
        data.setValue(selectedQuantity, forKey: "SelectedQuantity")
        
        data.write(toFile: fileURL.path, atomically: true)
    }
    
}
