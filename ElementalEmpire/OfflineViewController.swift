//
//  OfflineViewController.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 11/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import BigInt
import GoogleMobileAds
import PersonalizedAdConsent

class OfflineViewController: UIViewController
{

    @IBOutlet var consentButton: UIButton!
    @IBOutlet var displayView: UIView!
    @IBOutlet var timeOfflineLabel: UILabel!
    @IBOutlet var energyEarnedLabel: UILabel!
    
    @IBOutlet var doubleButton: UIButton!
    @IBOutlet var loadingAddSpinner: UIActivityIndicatorView!
    
    var offlineEnergy: BigInt!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        GADRewardBasedVideoAd.sharedInstance().delegate = self
        
        if GADRewardBasedVideoAd.sharedInstance().isReady
        {
            loadingAddSpinner.isHidden = true
        } else {
            loadingAddSpinner.startAnimating()
        }
        
        // Do any additional setup after loading the view.
        
        let now = NSDate()
        let old = ProgressSave.shared.lastDateClosed
        
        let difference = now.timeIntervalSince(old as Date)
        let date = DateInterval(start: old as Date, end: now as Date)
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .abbreviated
        formatter.allowedUnits = [.second, .minute, .hour, .day]
        let string = formatter.string(from: difference)!
        timeOfflineLabel.text = String("Time Offline: \(string)")
        timeOfflineLabel.adjustsFontSizeToFitWidth = true
        
        offlineEnergy = ProgressSave.shared.power * BigInt(difference)
        let offlineEnergyString = ProgressSave.shared.formatBigInt(bigInt: offlineEnergy)
        energyEarnedLabel.text = String("Energy Earned: \(offlineEnergyString)J")

        energyEarnedLabel.adjustsFontSizeToFitWidth = true

        
        let timeOffline = NSDate(timeIntervalSinceReferenceDate: difference)
        
        if PACConsentInformation.sharedInstance.isRequestLocationInEEAOrUnknown
        {
            consentButton.alpha = 1
        } else {
            consentButton.removeFromSuperview()
        }
    }
    
    @IBAction func collectButtonPressed(_ sender: Any)
    {
        ProgressSave.shared.energy += offlineEnergy
        
        Factory.shared.animateUIButton(button: sender as! UIButton)
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)
        ProgressSave.shared.saveChanges()

        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let vc = UIApplication.shared.windows.first!.rootViewController as? UINavigationController
            {                
                let gvc = vc.children[0] as! GameViewController
                gvc.giveOffline()
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {self.dismiss(animated: true, completion: {self.elementInfo()})})
        
    }
    
    @IBAction func doubleButtonPressed(_ sender: Any)
    {        
        Factory.shared.animateUIButton(button: sender as! UIButton)
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)
        if GADRewardBasedVideoAd.sharedInstance().isReady
        {
            doubleButton.isEnabled = false
        
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if GADRewardBasedVideoAd.sharedInstance().isReady
            {
                if let bp = SKTAudio.sharedInstance().backgroundMusicPlayer { bp.pause() }
                GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func buttonDown(_ sender: UIButton)
    {

    }
    
    @IBAction func buttonCancel(_ sender: UIButton)
    {
    }
    
    
    func elementInfo()
    {        
        if let vc = UIApplication.shared.windows.first!.rootViewController as? UINavigationController
        {            
            let gvc = vc.children[0] as! GameViewController
            gvc.displayElementInfo()
        }
    }
    

}
extension OfflineViewController: GADRewardBasedVideoAdDelegate
{
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didRewardUserWith reward: GADAdReward)
    {
        offlineEnergy *= 2
        ProgressSave.shared.saveChanges()

    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd)
    {
        if let bp = SKTAudio.sharedInstance().backgroundMusicPlayer { bp.play() }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {self.energyEarnedLabel.text = String("\(ProgressSave.shared.formatBigInt(bigInt: self.offlineEnergy))J")})
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {ProgressSave.shared.energy += self.offlineEnergy
            if let vc = UIApplication.shared.windows.first!.rootViewController as? UINavigationController
            {                
                let gvc = vc.children[0] as! GameViewController
                gvc.giveOffline()
            }
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {self.dismiss(animated: true, completion: {self.elementInfo()})})
        
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(), withAdUnitID: Factory.shared.freeAdID)
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd: GADRewardBasedVideoAd)
    {
        loadingAddSpinner.isHidden = true
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didFailToLoadWithError error: Error)
    {
        loadingAddSpinner.isHidden = true
        doubleButton.setTitle("Error Loading Ad", for: .normal)
        
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(), withAdUnitID: Factory.shared.doubleAdID)
    }
    
    func reloadAd()
    {
        GADMobileAds.sharedInstance()
    }
    
}
