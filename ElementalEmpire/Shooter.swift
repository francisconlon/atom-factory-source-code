//
//  Shooter.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 12/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import SpriteKit

class Shooter: SKNode
{
    var leftRail: SKSpriteNode!
    var rightRail: SKSpriteNode!
    var ballRadius: CGFloat!
    var currentBalls = [BallNode]()
    var specialSpawned = false
    var progressSave = ProgressSave.shared
    
    override init()
    {
        super.init()
        
        rightRail = SKSpriteNode(color: UIColor.white, size: CGSize(width: 10, height: 180))
        rightRail.anchorPoint = CGPoint(x: 0, y: 0)
        addChild(rightRail)
        rightRail.position.x += 25
        
        leftRail = SKSpriteNode(color: UIColor.white, size: CGSize(width: 10, height: 180))
        leftRail.anchorPoint = CGPoint(x: 1, y: 0)
        addChild(leftRail)
        leftRail.position.x -= 25
        
        let s = SKShapeNode(circleOfRadius: 5)
        s.lineWidth = 0
        s.fillColor = UIColor.white
        s.position = CGPoint(x: rightRail.frame.size.width / 2, y: 180)
        rightRail.addChild(s)
        
        let l = SKShapeNode(circleOfRadius: 5)
        l.lineWidth = 0
        l.fillColor = UIColor.white
        l.position = CGPoint(x:  -(leftRail.frame.size.width / 2), y: 180)
        leftRail.addChild(l)
        
        ballRadius = (rightRail.position.x - leftRail.position.x - 4) / 2
                
        let noOfBalls = Int(4)
        
        var positionIndex = ballRadius!
        for i in 1...noOfBalls
        {
            let ball = BallNode(special: false, radius: ballRadius)
            ball.position.y = positionIndex
            addChild(ball)
            currentBalls.append(ball)
            positionIndex += (ballRadius * 2)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func shootBall()
    {
        let gameScene = scene! as! GameScene
        
        let ballToShoot = currentBalls.last!
        ballToShoot.isPaused = false
        let moveAction = SKAction.move(to: scene!.convert(CGPoint.zero, to: self), duration: 0.2)
        moveAction.timingMode = .easeIn
        let sequence = SKAction.sequence([moveAction, SKAction.removeFromParent()])
        ballToShoot.run(sequence)
        let rotateAction = SKAction.rotate(byAngle: CGFloat(360).degreesToRadians(),
                                           duration: moveAction.duration)
        rotateAction.timingMode = .easeIn
        ballToShoot.run(rotateAction)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.21) {
            let emitterNodeNucleus = SKEmitterNode(fileNamed: "NucleusEmission.sks")!
            emitterNodeNucleus.targetNode = self.scene!
            emitterNodeNucleus.zPosition = 20
            emitterNodeNucleus.position = CGPoint(x: 0, y: 0)
            self.scene!.addChild(emitterNodeNucleus)
            emitterNodeNucleus.run(SKAction.sequence([SKAction.wait(forDuration: 2), SKAction.removeFromParent()]))
        }
        
        currentBalls.popLast()
        
        let position = currentBalls.first!.position
        let newPosition = CGPoint(x: 0, y: -(currentBalls.first!.calculateAccumulatedFrame().size.height) / 2)
        
        var special = false
        if !specialSpawned && progressSave.energy >= progressSave.target && gameScene.currentElementSprite.upgrading == false && gameScene.elementsIndex != Elements.shared.elements.count - 1
        {
            special = true
            specialSpawned = true
        }
        
        let ball = BallNode(special: special, radius: ballRadius)
        ball.position = newPosition
        addChild(ball)
        currentBalls.insert(ball, at: 0)
        
        if ballToShoot.special
        {
            let gameScene = scene as! GameScene
            gameScene.upgradeElement()
            specialSpawned = false
            self.run(SKAction.playSoundFileNamed("upgradeshot.wav", waitForCompletion: false))
        } else {
            self.run(SKAction.playSoundFileNamed("buttonpress.wav", waitForCompletion: false))
        }
        
        for i in 1...currentBalls.count
        {
            let ball = currentBalls[i-1]
            let positionY = ((ball.calculateAccumulatedFrame().size.height) * CGFloat(i - 1)) + ball.calculateAccumulatedFrame().size.height/2
            print(positionY)
            ball.isPaused = false
            
            ball.removeAction(forKey: "move")
            
            let action = SKAction.moveTo(y: positionY, duration: 0.1)
            action.timingMode = .easeInEaseOut
            ball.run(action, withKey: "move")
        }
    }
    
}
