//
//  BuildingsViewController.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 07/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import BigInt

class BuildingsViewController: UIViewController
{
    var progressSave = ProgressSave.shared
    
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var LargeQuantityButton: UIButton!
    @IBOutlet var MediumQuantityButton: UIButton!
    @IBOutlet var SmallQuantityButton: UIButton!
    
    var currentQuantity: BigInt = 1
    
    @IBOutlet var storeTableView: UITableView!
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        storeTableView.delegate = self
        storeTableView.dataSource = self
        // Do any additional setup after loading the view.
        currentQuantity = BigInt(ProgressSave.shared.selectedQuantity)

        
        SmallQuantityButton.layer.cornerRadius = 5
        MediumQuantityButton.layer.cornerRadius = 5
        LargeQuantityButton.layer.cornerRadius = 5
        doneButton.layer.cornerRadius = 5
        
        doneButton.backgroundColor = Factory.shared.buttonColor
        SmallQuantityButton.backgroundColor = Factory.shared.buttonColor
        MediumQuantityButton.backgroundColor = Factory.shared.buttonColor
        LargeQuantityButton.backgroundColor = Factory.shared.buttonColor
        
        Factory.shared.smallButtonFrame = SmallQuantityButton.frame
        Factory.shared.mediumButtonFrame = MediumQuantityButton.frame
        Factory.shared.largeButtonFrame = LargeQuantityButton.frame
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        switch currentQuantity
        {
        case 1:
            Factory.shared.animateQuantityButton(button: SmallQuantityButton)
        case 10:
            Factory.shared.animateQuantityButton(button: MediumQuantityButton)
        case 100:
            Factory.shared.animateQuantityButton(button: LargeQuantityButton)
        default:
            break
        }
    }
    

    @IBAction func donePressed(_ sender: Any)
    {
        Factory.shared.animateUIButton(button: sender as! UIButton)
        
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)

        
        progressSave.selectedQuantity = Int(currentQuantity)
        
        ProgressSave.shared.saveChanges()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + Factory.shared.duration) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func quantityChanged(_ sender: Any)
    {
        let tagSender = sender as! UIButton
        
        switch currentQuantity
        {
        case 1:
            if tagSender.tag != 0
            {
                Factory.shared.deanimateQuantityButton(button: SmallQuantityButton)
                Factory.shared.animateQuantityButton(button: tagSender)
                
                if tagSender.tag == 1
                {
                    currentQuantity = 10
                } else if tagSender.tag == 2 {
                    currentQuantity = 100
                }
                storeTableView.reloadSections(IndexSet(integer: 0), with: .right)
            }
        case 10:
            if tagSender.tag != 1
            {
                Factory.shared.deanimateQuantityButton(button: MediumQuantityButton)
                Factory.shared.animateQuantityButton(button: tagSender)
                
                if tagSender.tag == 0
                {
                    currentQuantity = 1
                } else if tagSender.tag == 2 {
                    currentQuantity = 100
                }
                storeTableView.reloadSections(IndexSet(integer: 0), with: .right)
            }
        case 100:
            if tagSender.tag != 2
            {
                Factory.shared.deanimateQuantityButton(button: LargeQuantityButton)
                Factory.shared.animateQuantityButton(button: tagSender)
                
                if tagSender.tag == 0
                {
                    currentQuantity = 1
                } else if tagSender.tag == 1 {
                    currentQuantity = 10
                }
                storeTableView.reloadSections(IndexSet(integer: 0), with: .right)
            }
        default:
            break
        }
        
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)

        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BuildingsViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {

            return progressSave.unlockedBuildings + 1

        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row != tableView.numberOfRows(inSection: 0) - 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShopViewCell", for: indexPath) as! ShopViewCell
            
            cell.transparentBackground.layer.cornerRadius = cell.frame.size.height * 0.05
            cell.transparentBackground.backgroundColor = Factory.shared.cellColor
            let building = Buildings.shared.buildings[indexPath.row]
            cell.nameLabel.text = building.name
            cell.hexagonImageView.image = UIImage(named: "categories\(building.type).png")
            
            let priceCurrent = building.priceForQuantity(quantity: BigInt(currentQuantity))
            let pricePrice = progressSave.formatBigInt(bigInt: priceCurrent)
            let powerLabel = progressSave.formatBigInt(bigInt: (building.power * currentQuantity))
            let ownedLabel = progressSave.formatBigInt(bigInt: progressSave.numberOwned[indexPath.row])
            
            cell.priceLabel.text = String("Cost: \(pricePrice)J")
            cell.powerLabel.text = String("Output: \(powerLabel)W")
            cell.numberOwned.text = String("x\(ownedLabel)")
            
            cell.priceLabel.adjustsFontSizeToFitWidth = true
            cell.powerLabel.adjustsFontSizeToFitWidth = true
            cell.numberOwned.adjustsFontSizeToFitWidth = true
            cell.nameLabel.adjustsFontSizeToFitWidth = true
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MysteryViewCell", for: indexPath) as! MysteryViewCell
            cell.background.layer.cornerRadius = cell.frame.size.height * 0.05
            return cell
        }
    }
}

extension BuildingsViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let nvx = self.presentingViewController as! UINavigationController
        let gvc = nvx.viewControllers.first as! GameViewController
        
        print("selected, ", indexPath.row)
        if tableView.cellForRow(at: indexPath) is ShopViewCell
        {
            let building = Buildings.shared.buildings[indexPath.row]
            if progressSave.energy > building.priceForQuantity(quantity: currentQuantity)
            {
                if progressSave.numberOwned[indexPath.row] == 0 &&  progressSave.unlockedBuildings < Buildings.shared.buildings.count - 1
                {
                    progressSave.unlockedBuildings += 1
                    tableView.insertRows(at: [IndexPath(row: progressSave.unlockedBuildings, section: 0)], with: .none)
                    tableView.reloadRows(at: [IndexPath(row: tableView.numberOfRows(inSection: 0) - 2, section: 0)], with: .none)
                }
                
                progressSave.energy -= building.priceForQuantity(quantity: currentQuantity)
                progressSave.power += BigInt(building.power * currentQuantity * BigInt(progressSave.buildingsMultiplierIndex[indexPath.row]))
                progressSave.numberOwned[indexPath.row] += currentQuantity
                building.increasePriceFor(count: currentQuantity)
                tableView.reloadRows(at: [indexPath], with: .none)
                
                let cell = tableView.cellForRow(at: indexPath) as! ShopViewCell
                
                UIView.animate(withDuration: 0.1) {
                    cell.transparentBackground.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    UIView.animate(withDuration: 0.1) {
                        cell.transparentBackground.transform = CGAffineTransform(scaleX: 1, y: 1)
                    }
                }
                
                switch currentQuantity
                {
                case 1:
                    gvc.updateForSmallQuantity()
                    SKTAudio.sharedInstance().playSoundEffect("combo1.m4a")
                case 10:
                    gvc.updateForMediumQuantity()
                    SKTAudio.sharedInstance().playSoundEffect("combo1.m4a")
                case 100:
                    gvc.updateForLargeQuantity()
                    SKTAudio.sharedInstance().playSoundEffect("combo2.m4a")
                default:
                    break
                }
                
            }
        }
    }
    
    
}

class ShopViewCell: UITableViewCell
{
    @IBOutlet var transparentBackground: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var numberOwned: UILabel!
    @IBOutlet var powerLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var hexagonImageView: UIImageView!
}

class MysteryViewCell: UITableViewCell
{

    @IBOutlet var background: UIView!
}
