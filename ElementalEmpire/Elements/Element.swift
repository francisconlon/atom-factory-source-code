//
//  Element.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 06/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation

class Element: NSObject
{
    var name: String
    var symbol: String
    var atomicNumber: Int
    var protons: Int
    var neutrons: Int
    var electrons: [Int]
    
    init(name: String, symbol: String, number: Int, protons: Int, neutrons: Int, electrons: [Int])
    {
        self.name = name
        self.symbol = symbol
        self.atomicNumber = number
        self.protons = protons
        self.neutrons = neutrons
        self.electrons = electrons
        print(symbol, protons, neutrons, electrons.enumerated())
    }
    
}
