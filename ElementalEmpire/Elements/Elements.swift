//
//  Elements.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 06/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation

class Elements
{
    static let shared = Elements()
    
    var elements: [Element]
    
    private init()
    {
        elements = [Element]()
        let path = Bundle.main.url(forResource: "ElementsPropertyList", withExtension: ".plist")
        let elementsArray = NSArray(contentsOfFile: path!.path) as! [NSDictionary]
        for elementDict in elementsArray
        {
            let nameA = elementDict["Name"] as! String
            let symbolA = elementDict["Symbol"] as! String
            let numberAS = elementDict["AtomicNumber"] as! String
            let numberA = Int(numberAS)!
            let proton = elementDict["Protons"] as! Int
            let neutron = elementDict["Neutrons"] as! Int
            let electron = elementDict["Electrons"] as! [Int]
            
            let element = Element(name: nameA, symbol: symbolA, number: numberA, protons: proton, neutrons: neutron, electrons: electron)
            elements.append(element)
        }
    }
    
}
