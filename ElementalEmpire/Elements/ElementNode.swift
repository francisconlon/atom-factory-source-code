//
//  ElementNode.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 10/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import Foundation

class ElementNode: SKNode
{
    var nucleus: Nucleus!
    var element: Element
    var electronRings = [ElectronRing]()
    var size: CGSize
    
    var upgrading = false
    
    init(size: CGSize, elementIndex: Int)
    {
        
        self.size = size
        element = Elements.shared.elements[elementIndex]
        
        super.init()
        
        nucleus = Nucleus(protons: element.protons, neutrons: element.neutrons, radius: size.width * 0.1)
        addChild(nucleus)
        
        let electronRingNumber = element.electrons.count
        let integerElectronRings = electronRingNumber + 1
        
        let frameSize = CGSize(width: size.width * 0.2,
                               height: size.width * 0.2)
        
        let electronWidth = (size.width - frameSize.width) / 2
        let step = electronWidth / CGFloat(integerElectronRings)
        
        var indexPosition = (frameSize.width / 2)
        
        let scaleStep = 1.0 / CGFloat(element.electrons.count - 1)
        var scale:CGFloat = 0.0
        
        for i in 0..<element.electrons.count
        {
            let finalScale = (0.6 * scale) + 0.4
            scale += scaleStep
            
            let electronShell = element.electrons[i]
            print(indexPosition, size.width, nucleus.frame.size.width)
            indexPosition += step
            let electronRing = ElectronRing(radius: size.width/2, electrons: electronShell, noOfRings: electronRingNumber)
            addChild(electronRing)
            
            
            electronRing.setScale(finalScale)
            electronRing.compensateForScaleFactor(scaleFactor: finalScale, animated: false, animationDuration: 0.0)
            
            electronRings.append(electronRing)
            
            var rotateAction = SKAction()
            if i % 2 == 0
            {
                rotateAction = SKAction.rotate(byAngle: CGFloat(360).degreesToRadians(), duration: 5)
            } else {
                rotateAction = SKAction.rotate(byAngle: CGFloat(-360).degreesToRadians(), duration: 5)
            }
            
            electronRing.isPaused = false
            electronRing.run(SKAction.repeatForever(rotateAction), withKey: "rotate")
        }
    }
    
    func destroy()
    {
        self.run(SKAction.scale(to: 0, duration: 0.5), withKey: "shrink")
        
        let emitterNode = SKEmitterNode(fileNamed: "NewNucleusParticleBlue.sks")!
        emitterNode.targetNode = self.scene!
        emitterNode.zPosition = 20
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
        {
            self.scene!.addChild(emitterNode)
            emitterNode.run(SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.removeFromParent()]))
        }
        
        
        let emitterNodeRed = SKEmitterNode(fileNamed: "NewNucleusParticleRed.sks")!
        emitterNodeRed.targetNode = self.scene!
        emitterNodeRed.zPosition = 20
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
        {
            self.scene!.addChild(emitterNodeRed)
            emitterNodeRed.run(SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.removeFromParent()]))
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            self.removeAllChildren()
            self.removeFromParent()
        }
        
    }
    
    func upgradeTo(elementIndex: Int)
    {
        upgrading = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {self.upgrading = false})
        
        let currentElement = Elements.shared.elements[elementIndex - 1]
        let nextElement = Elements.shared.elements[elementIndex]
        element = Elements.shared.elements[elementIndex]
        
        for i in 0..<nextElement.electrons.count
        {
            print("upgrade from \(currentElement.name) to \(nextElement.name)")
            if i >= currentElement.electrons.count
            {
                print("upgrade new ring \(nextElement.electrons[i])")
                // Shrink old rings
                let scaleStep = 1.0 / CGFloat(nextElement.electrons.count - 1)
                var scale:CGFloat = 0.0
                
                for ring in electronRings
                {
                    let finalScale = (0.6 * scale) + 0.4
                    ring.run(SKAction.scale(to: finalScale, duration: 0.5))
                    ring.compensateForScaleFactor(scaleFactor: finalScale, animated: true, animationDuration: 0.5)
                    
                    scale += scaleStep
                }
                
                // add new ring
                let newRing = ElectronRing(radius: size.width/2, electrons: nextElement.electrons[i], noOfRings: 1)
                addChild(newRing)
                electronRings.append(newRing)
                newRing.alpha = 0
                newRing.run(SKAction.sequence([SKAction.wait(forDuration: 0.5), SKAction.fadeIn(withDuration: 0.5)]))
            
                var rotateAction = SKAction()
                if i % 2 == 0
                {
                    rotateAction = SKAction.rotate(byAngle: CGFloat(360).degreesToRadians(), duration: 5)
                } else {
                    rotateAction = SKAction.rotate(byAngle: CGFloat(-360).degreesToRadians(), duration: 5)
                }
                
                newRing.run(SKAction.repeatForever(rotateAction), withKey: "rotate")
                
            } else {
                let previousRing = currentElement.electrons[i]
                let nextRing = nextElement.electrons[i]
                let difference = nextRing - previousRing
                print("upgrade adding \(difference) to ring \(i)")
                if difference > 0
                {
                    electronRings[i].addElectrons(count: difference)
                }
            }
            
            
        }
        
        nucleus.upgradeTo(index: elementIndex)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}
