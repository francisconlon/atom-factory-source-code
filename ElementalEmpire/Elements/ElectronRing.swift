//
//  ElectronRing.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 10/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import SpriteKit

class ElectronRing: SKNode
{
    var currentElectrons = [SKSpriteNode]()
    var radius: CGFloat
    var noOfRings: Int
    var lineWidth: CGFloat
    var currentLineWidth: CGFloat!
    var electronRing: SKShapeNode!
    var electronRadius: CGFloat!
    var electronScaleFactor: CGFloat!
    
    init(radius: CGFloat, electrons: Int, noOfRings: Int)
    {
        self.radius = radius
        self.noOfRings = noOfRings
        self.lineWidth = radius * 0.015
        self.currentLineWidth = self.lineWidth
        self.electronScaleFactor = 1.0
        
        super.init()
                
        electronRing = SKShapeNode(circleOfRadius: radius)
        electronRing.lineWidth = lineWidth
        electronRing.fillColor = UIColor.clear
        electronRing.strokeColor = UIColor.white
        electronRing.alpha = 0.9
        addChild(electronRing)
        
        let step: CGFloat = 360/CGFloat(electrons)
        var index: CGFloat = 0
        
        var angles = [CGFloat]()
        
        for i in 1...electrons
        {
            angles.append(index)
            index += step
        }
                
        let points = [CGPoint()]
        for angle in angles
        {
            let vector = CGVector(angle: angle.degreesToRadians())
            let lengthVector = vector * radius
            let roundedPoint = CGPoint(x: Int(lengthVector.dx), y: Int(lengthVector.dy))
            
            electronRadius = radius * 0.04
            let electron = SKSpriteNode(texture: SKTexture(imageNamed: "hexagon.png"),
                                    color: SKColor.yellow,
                                    size: CGSize(width: electronRadius * 2,
                                                 height: electronRadius * 2))
            electron.colorBlendFactor = 1
            electron.position = roundedPoint
            electron.zPosition = 1
            let angleVector = roundedPoint - CGPoint.zero
            electron.zRotation = CGFloat(angleVector.angle.radiansToDegrees() - 90).degreesToRadians()
            addChild(electron)
            currentElectrons.append(electron)
        }
    }
    
    func addElectrons(count: Int)
    {
        let newElectrons = currentElectrons.count + count
        
        let step: CGFloat = 360/CGFloat(newElectrons)
        var index: CGFloat = 0
        
        var angles = [CGFloat]()
        var points = [CGPoint]()
        
        for i in 0..<newElectrons
        {
            angles.append(index)
            
            
            let vector = CGVector(angle: index.degreesToRadians())
            let lengthVector = vector * radius
            let roundedPoint = CGPoint(x: Int(lengthVector.dx), y: Int(lengthVector.dy))
            points.append(roundedPoint)
            
            index += step
            
            // if already exists then move
            if i < currentElectrons.count
            {
                let electronToMove = currentElectrons[i]

                var array = [SKAction]()
                
                let step = (roundedPoint - electronToMove.position) / 4
                var point = electronToMove.position + step
                for _ in 1...3
                {
                    let vectorPoint = CGPoint(vector: (CGVector(point: point).normalized() * radius))
                    let action = SKAction.move(to: vectorPoint, duration: 0.1)
                    array.append(action)
                    point += step
                }
                let action = SKAction.move(to: roundedPoint, duration: 0.1)
                array.append(action)
                
                electronToMove.run(SKAction.sequence(array))
                
                let angleVector = roundedPoint - CGPoint.zero
                electronToMove.run(SKAction.rotate(toAngle: CGFloat(angleVector.angle.radiansToDegrees() - 90).degreesToRadians(),
                                                   duration: 0.3))

            } else {
                
                //electronRadius = radius * 0.05
                let electron = SKSpriteNode(texture: SKTexture(imageNamed: "hexagon.png"),
                                            color: SKColor.yellow,
                                            size: CGSize(width: electronRadius * 2,
                                                         height: electronRadius * 2))
                electron.colorBlendFactor = 1
                electron.position = roundedPoint
                electron.zPosition = 1
                let angleVector = roundedPoint - CGPoint.zero
                electron.zRotation = CGFloat(angleVector.angle.radiansToDegrees() - 90).degreesToRadians()
                addChild(electron)
                currentElectrons.append(electron)
                electron.setScale(electronScaleFactor)
                electron.alpha = 0
                electron.run(SKAction.sequence([SKAction.wait(forDuration: 0.4), SKAction.fadeIn(withDuration: 0.2)]))
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    let emitterNodeElectron = SKEmitterNode(fileNamed: "NewElectronParticle.sks")!
                    emitterNodeElectron.targetNode = self.scene!
                    emitterNodeElectron.zPosition = 20
                    electron.addChild(emitterNodeElectron)
                    emitterNodeElectron.run(SKAction.sequence([SKAction.wait(forDuration: 2), SKAction.removeFromParent()]))
                }
            }
            
        }
    }
    
    func compensateForScaleFactor(scaleFactor: CGFloat, animated: Bool, animationDuration: Double)
    {
        let nextLineWidth = lineWidth / scaleFactor
        electronScaleFactor = 1/scaleFactor
        
        if animated == false
        {
            electronRing.lineWidth = nextLineWidth
            
            for electron in currentElectrons
            {
                electron.setScale(electronScaleFactor)
            }
        } else if animated == true {
            let difference = nextLineWidth - currentLineWidth
            if difference > 0
            {
                let step = difference / 5
                var actions = [SKAction]()
                for i in 1...5
                {
                    let newStep = step * CGFloat(i)
                    let action = SKAction.afterDelay(animationDuration / 5, runBlock: {self.electronRing.lineWidth = self.currentLineWidth + newStep})
                    actions.append(action)
                }
                self.run(SKAction.sequence(actions))
            }
            
            for electron in currentElectrons
            {
                electron.run(SKAction.scale(to: electronScaleFactor, duration: animationDuration))
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}
