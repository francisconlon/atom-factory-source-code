//
//  Nucleus.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 10/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import SpriteKit

class Nucleus: SKNode
{
    
    var particleArray = [SKSpriteNode]()
    var radius: CGFloat
    
    init(protons: Int, neutrons: Int, radius: CGFloat)
    {
        self.radius = radius
        
        super.init()

        
        self.zPosition = 10
        
        particleArray = getNucleusNodes(protons: protons, neutrons: neutrons, radius: radius)
        
        for layer in particleArray
        {
            addChild(layer)
        }
        
        if particleArray.count == 1
        {
            particleArray[0].setScale(2)
        }
    }
    

    func upgradeTo(index: Int)
    {
        let protons = Elements.shared.elements[index].protons
        let neutrons = Elements.shared.elements[index].neutrons
        
        let array = getNucleusNodes(protons: protons, neutrons: neutrons, radius: radius)
        
        for layer in particleArray
        {
            layer.run(SKAction.sequence([SKAction.scale(to: 0, duration: 0.5),
                                         SKAction.removeFromParent()]))
        }
        
        for layer in array
        {
            addChild(layer)
            layer.setScale(0)
            
            if array.count == 1 && (protons + neutrons) < 39
            {
                layer.run(SKAction.scale(to: 2, duration: 0.5))
            } else {
                layer.run(SKAction.scale(to: 1.1, duration: 0.5))
            }
        }
        
        // Do particle effects
        
        let emitterNode = SKEmitterNode(fileNamed: "NewNucleusParticleBlue.sks")!
        emitterNode.targetNode = self.scene!
        emitterNode.zPosition = 20
        addChild(emitterNode)
        emitterNode.run(SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.removeFromParent()]))
        
        let emitterNodeRed = SKEmitterNode(fileNamed: "NewNucleusParticleRed.sks")!
        emitterNodeRed.targetNode = self.scene!
        emitterNodeRed.zPosition = 20
        addChild(emitterNodeRed)
        emitterNodeRed.run(SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.removeFromParent()]))
        
        particleArray = array
    }
    
    func getNucleusNodes(protons: Int, neutrons: Int, radius: CGFloat) -> [SKSpriteNode]
    {
        
        var totalSubatomCount: Int = (protons + neutrons) / 2
        var scale: CGFloat = 1.0
        var createdIndex = 0
        var array = [SKSpriteNode]()
        
        let topBoiCount = ((protons + neutrons) / 2) % 19
        if totalSubatomCount > 100 {
            let main = SKSpriteNode(texture: SKTexture(imageNamed: "nucleusmain-1.png"),
                                    color: UIColor.clear,
                                    size: CGSize(width: radius * 2,
                                                 height: radius * 2))
            array.append(main)
            main.zPosition = 1
            totalSubatomCount -= 50
            createdIndex += 1
        }
        
        if totalSubatomCount > 75
        {
            let main = SKSpriteNode(texture: SKTexture(imageNamed: "nucleusmain-2.png"),
                                    color: UIColor.clear,
                                    size: CGSize(width: radius * 2,
                                                 height: radius * 2))
            array.append(main)
            main.zPosition = 2
            totalSubatomCount -= 50
            createdIndex += 1
        }
        
        if totalSubatomCount >= 19
        {
            let main = SKSpriteNode(texture: SKTexture(imageNamed: "nucleusmain-3.png"),
                                    color: UIColor.clear,
                                    size: CGSize(width: radius * 2,
                                                 height: radius * 2))
            array.append(main)
            main.zPosition = 3
            createdIndex += 1
        }
        
        if topBoiCount >= 0
        {
            let main = SKSpriteNode(texture: SKTexture(imageNamed: "nucleusremainder-\(topBoiCount + 1).png"),
                                    color: UIColor.clear,
                                    size: CGSize(width: radius * 2,
                                                 height: radius * 2))
            array.append(main)
            main.zPosition = 4
            createdIndex += 1
        }
        return array
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}
