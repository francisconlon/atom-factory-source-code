//
//  CreditsViewController.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 31/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit

class CreditsViewController: UIViewController
{

    @IBOutlet var contentView: UIView!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var creditScrollView: UIScrollView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        doneButton.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.60, alpha:1.0)
        doneButton.layer.cornerRadius = 5
        contentView.layer.cornerRadius = contentView.frame.size.width * 0.05
        
        let credits = NSArray(contentsOfFile: Bundle.main.path(forResource: "CreditsPropertyList", ofType: ".plist")!)!
        var positionYCredits = 0
        let width = creditScrollView.frame.size.width
        for credit in credits
        {
            let label = UILabel(frame: CGRect(x: 0,
                                              y: positionYCredits,
                                              width: Int(width),
                                              height: 20))
            label.font = UIFont(name: "SulfurPoint-Regular", size: 16)
            label.text = String("\(credit)")
            label.textColor = UIColor.black
            label.adjustsFontSizeToFitWidth = true
            creditScrollView.addSubview(label)
            positionYCredits += Int(label.frame.size.height)
        }
        creditScrollView.contentSize = CGSize(width: Int(width),
                                               height: positionYCredits)
    }
    

    @IBAction func doneButtonPressed(_ sender: UIButton)
    {
        Factory.shared.animateUIButton(button: sender)
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + Factory.shared.duration) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
