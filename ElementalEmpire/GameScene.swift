//
//  GameScene.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 05/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit
import BigInt

class GameScene: SKScene {
    
    var progressUpdater: Progress!
    
    var spawnSpecialBallFlag = false
    
    var shooter: Shooter!
    
    var elements = Elements.shared.elements
    var elementsIndex = ProgressSave.shared.currentElement {
        didSet {
            progressSave.currentElement = elementsIndex
        }
    }
    var currentElement: Element!
    var currentElementSprite: ElementNode!
    var visibleScreen: CGRect!
    
    var progressSave = ProgressSave.shared
    
    var currentEnergy: BigInt = 0 {
        didSet {

        }
    }
    
    
    func upgradeElement()
    {
        if progressSave.energy >= progressSave.target
        {
            if elementsIndex < elements.count - 1
            {
                elementsIndex += 1
            
                currentElement = elements[elementsIndex]
                
                progressSave.target = progressSave.target * BigInt(2.5)
                
                let a = progressSave.energy * 10000
                let b = progressSave.target * 10
                let c = a.quotientAndRemainder(dividingBy: b).quotient / 2
                
                var int = Int64()
                if c > Int64.max
                {
                    int = Int64.max
                } else {
                    int = Int64(c)
                }
                
                progressUpdater.completedUnitCount = int
                
                print(progressSave.target, currentEnergy, currentElement.name)
                currentEnergy = 0
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3)
                {
                    self.run(SKAction.playSoundFileNamed("upgradesound1.wav", waitForCompletion: false))
                    self.run(SKAction.playSoundFileNamed("upgradesound2.wav", waitForCompletion: false))
                    self.upgradeElementSprite()
                }
                
                
                
            } else if elementsIndex >= elements.count - 1 {
                
            }
        }
    }
    
    override func didMove(to view: SKView)
    {
        visibleScreen = getVisibleScreen(sceneWidth: scene!.size.width,
                                         sceneHeight: scene!.size.height,
                                         viewWidth: view.bounds.width,
                                         viewHeight: view.bounds.height)
        setupElementSprite()
        setupShooter()
        setupBackground()
    }
    
    func giveOffline()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            let emitterNodeEnergy = SKEmitterNode(fileNamed: "EnergyAwardParticle.sks")!
            emitterNodeEnergy.targetNode = self.scene!
            emitterNodeEnergy.zPosition = 20
            emitterNodeEnergy.position = CGPoint(x: -(self.visibleScreen.width / 4),
                                                 y: (self.scene!.size.height / 2) * 0.88)
            self.addChild(emitterNodeEnergy)
            emitterNodeEnergy.run(SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.removeFromParent()]))
            self.run(SKAction.playSoundFileNamed("unlock.wav", waitForCompletion: false))
        }
    }
    
    func splashPower()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            let emitterNodeEnergy = SKEmitterNode(fileNamed: "EnergyAwardParticle.sks")!
            emitterNodeEnergy.targetNode = self.scene!
            emitterNodeEnergy.zPosition = 20
            emitterNodeEnergy.position = CGPoint(x: (self.visibleScreen.width / 4),
                                                 y: (self.scene!.size.height / 2) * 0.88)
            self.addChild(emitterNodeEnergy)
            emitterNodeEnergy.run(SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.removeFromParent()]))
        }
    }
    
    func setupBackground()
    {
        let backgroundEmitter = SKEmitterNode(fileNamed: "BackgroundParticle.sks")!
        addChild(backgroundEmitter)
        
        let background = BackgroundNodeOverlay()
        addChild(background)
    }
    
    func setupShooter()
    {
        shooter = Shooter()
        shooter.position = CGPoint(x: 0, y: -scene!.size.height/2)
        addChild(shooter)
    }
    
    func upgradeElementSprite()
    {
        if currentElementSprite != nil
        {
            currentElementSprite.upgradeTo(elementIndex: elementsIndex)
            
            if let vc = view!.window?.rootViewController as? UINavigationController
            {
                let gvc = vc.children[0] as! GameViewController
                gvc.displayElementInfo()
            }
        }
    }
    
    func reset()
    {
        elementsIndex = 0
        currentElementSprite.destroy()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)
        {
            self.setupElementSprite()
            self.currentElementSprite.setScale(0)
            self.currentElementSprite.run(SKAction.scale(to: 1, duration: 0.5))
        }
        
        giveOffline()
        splashPower()
    }
    
    func setupElementSprite()
    {
        /*if currentElementSprite != nil
        {
            currentElementSprite.removeAllChildren()
            currentElementSprite.removeFromParent()
        }*/
        
        currentElement = elements[elementsIndex]
        
        let elementSize = CGSize(width: visibleScreen.width * 0.8,
                                 height: visibleScreen.width * 0.8)
        
        currentElementSprite = ElementNode(size: elementSize, elementIndex: elementsIndex)
        addChild(currentElementSprite)
        

    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        
        shooter.shootBall()
        
        
        let amount = BigInt(1 * progressSave.clickMultiplierIndex)
        print("amount", amount)
        currentEnergy += amount
        ProgressSave.shared.energy += amount
        
    }
    
    
    override func update(_ currentTime: TimeInterval)
    {
        if progressSave.energy >= progressSave.target
        {
            spawnSpecialBallFlag = true
        }
    }
    
    
    // MARK: - Helpers
    
    func getVisibleScreen(sceneWidth: CGFloat, sceneHeight: CGFloat, viewWidth: CGFloat, viewHeight: CGFloat) -> CGRect
    {
        var x: CGFloat = 0
        var y: CGFloat = 0
        var sWidth = sceneWidth
        var sHeight = sceneHeight
        
        let deviceAspectRatio = viewWidth/viewHeight
        let sceneAspectRatio = sceneWidth/sceneHeight
        
        //If the the device's aspect ratio is smaller than the aspect ratio of the preset scene dimensions, then that would mean that the visible width will need to be calculated
        //as the scene's height has been scaled to match the height of the device's screen. To keep the aspect ratio of the scene this will mean that the width of the scene will extend
        //out from what is visible.
        //The opposite will happen in the device's aspect ratio is larger.
        if deviceAspectRatio < sceneAspectRatio {
            let newSceneWidth: CGFloat = (sWidth * viewHeight) / sceneHeight
            let sceneWidthDifference: CGFloat = (newSceneWidth - viewWidth)/2
            let diffPercentageWidth: CGFloat = sceneWidthDifference / (newSceneWidth)
            
            //Increase the x-offset by what isn't visible from the lrft of the scene
            x = diffPercentageWidth * sceneWidth
            //Multipled by 2 because the diffPercentageHeight is only accounts for one side(e.g right or left) not both
            sWidth = sWidth - (diffPercentageWidth * 2 * sWidth)
        } else {
            let newSceneHeight: CGFloat = (sceneHeight * viewWidth) / sceneWidth
            let sceneHeightDifference: CGFloat = (newSceneHeight - viewHeight)/2
            let diffPercentageHeight: CGFloat = abs(sceneHeightDifference / (newSceneHeight))
            
            //Increase the y-offset by what isn't visible from the bottom of the scene
            y = diffPercentageHeight * sceneHeight
            //Multipled by 2 because the diffPercentageHeight is only accounts for one side(e.g top or bottom) not both
            sHeight = sHeight - (diffPercentageHeight * 2 * sHeight)
        }
        
        let visibleScreenOffset = CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(sWidth), height: CGFloat(sHeight))
        return visibleScreenOffset
    }
    
    
}
