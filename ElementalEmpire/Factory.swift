//
//  Factory.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 26/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import UIKit

class Factory
{
    static let shared = Factory()
    
    let doubleAdID = "ca-app-pub-8001969410865481/1838961325"
    let freeAdID = "ca-app-pub-8001969410865481/1096014855"
    
    let duration = 0.2
    
    let buttonPressSound = "buttonpress.wav"
    let buttonInvalidSound = "invalidbutton.wav"
    
    var smallButtonFrame: CGRect!
    var mediumButtonFrame: CGRect!
    var largeButtonFrame: CGRect!
    
    var buttonColor: UIColor = UIColor(red:1, green:1, blue:1.00, alpha:1)
    var cellColor: UIColor = UIColor(red:1, green:1, blue:1.00, alpha:0.9)
    
    
    func animateUIButton(button: UIButton)
    {
        //let button = sender as! UIButton
        let bigFrame = button.frame
        let smallFrame = CGRect(x: bigFrame.origin.x + (bigFrame.size.width * 0.1),
                                y: bigFrame.origin.y + (bigFrame.size.height * 0.1),
                                width: bigFrame.size.width * 0.8,
                                height: bigFrame.size.height * 0.8)
        UIView.animate(withDuration: 0.1,
                       animations: {button.frame = smallFrame}) { (completed) in
                        UIView.animate(withDuration: 0.07,
                                       animations: {button.frame = bigFrame}, completion: nil)
        }
    }
    
    func animateQuantityButton(button: UIButton)
    {
        //let button = sender as! UIButton
        /*let bigFrame = button.frame
        let smallFrame = CGRect(x: bigFrame.origin.x + (bigFrame.size.width * 0.05),
                                y: bigFrame.origin.y + (bigFrame.size.height * 0.05),
                                width: bigFrame.size.width * 0.9,
                                height: bigFrame.size.height * 0.9)
        UIView.animate(withDuration: 0.1,
                       animations: {button.bounds = smallFrame
                        button.backgroundColor = UIColor(red:0.09, green:0.79, blue:0.24, alpha:0.8)
        }) { (completed) in
                        
        }*/
        
        UIView.animate(withDuration: 0.2) {
            button.backgroundColor = UIColor(red:0.54, green:0.96, blue:0.26, alpha:1.0)
        }
    }
    
    func deanimateQuantityButton(button: UIButton)
    {
        var rect = CGRect()
        
        switch button.tag
        {
        case 0:
            rect = smallButtonFrame
        case 1:
            rect = mediumButtonFrame
        case 2:
            rect = largeButtonFrame
        default:
            break
        }
        
        
        UIView.animate(withDuration: 0.1,
                       animations: { //button.frame = rect
                        button.backgroundColor = self.buttonColor
        }) { (completed) in
            
        }
    }
    
}
