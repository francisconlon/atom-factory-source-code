//
//  UpgradesViewController.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 13/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import BigInt

class UpgradesViewController: UIViewController {

    @IBOutlet var upgradesTableView: UITableView!
    
    @IBOutlet var specialEnergyLabel: UILabel!
    
    @IBOutlet var doneButton: UIButton!
    @IBAction func morePressed(_ sender: Any)
    {
        //ProgressSave.shared.specialEnergy += 10
        //updateBalanceLabel()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        specialEnergyLabel.text = String("\(ProgressSave.shared.specialEnergy)")
        upgradesTableView.dataSource = self
        upgradesTableView.delegate = self
        upgradesTableView.allowsSelection = false
        upgradesTableView.reloadSections(IndexSet(arrayLiteral: 0,1), with: .none)
        
        doneButton.backgroundColor = Factory.shared.buttonColor
        doneButton.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    @IBAction func doneButtonPressed(_ sender: Any)
    {
        ProgressSave.shared.saveChanges()

        
        Factory.shared.animateUIButton(button: sender as! UIButton)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + Factory.shared.duration) {
            self.dismiss(animated: true, completion: nil)
        }
        
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func resetGame()
    {
        let nvc = self.presentingViewController as! UINavigationController
        let gvc = nvc.viewControllers.first! as! GameViewController
        self.dismiss(animated: true) {
            gvc.resetGame()
        }
        
        
    }
    
    @IBAction func showStore(_ sender: UIButton)
    {
        Factory.shared.animateUIButton(button: sender)
        
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)
        
        doneButton.isEnabled = false
        UIView.animate(withDuration: 0.2, animations: {
            self.doneButton.alpha = 0
        })
        
        self.performSegue(withIdentifier: "showStore", sender: self)
    }
    
    func updateBalanceLabel()
    {
        
        
        UIView.animate(withDuration: 0.2) {
            self.specialEnergyLabel.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            UIView.animate(withDuration: 0.15) {
                self.specialEnergyLabel.text = String("\(ProgressSave.shared.specialEnergy)")
                self.specialEnergyLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
    }
    
    func updatePurchase()
    {
        UIView.animate(withDuration: 0.2) {
            self.specialEnergyLabel.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            UIView.animate(withDuration: 0.15) {
                SKTAudio.sharedInstance().playSoundEffect("unlock.wav")
                self.specialEnergyLabel.text = String("\(ProgressSave.shared.specialEnergy)")
                self.specialEnergyLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
    }
    
}

extension UpgradesViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section {
        case 0:
            return 1
        case 1:
            return ProgressSave.shared.unlockedBuildings
        default:
            return 2
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UpgradeCell", for: indexPath) as! UpgradeViewCell
        
        switch indexPath.section
        {
        case 0:
            // Return Click Cell
            cell.buildingName.text = "Click"
            cell.setViewForIndex(index: ProgressSave.shared.clickMultiplierIndex)
            if ProgressSave.shared.clickMultiplierIndex >= 10
            {
                cell.upgradeButton.alpha = 0
                cell.upgradeButton.isEnabled = false
                cell.priceLabel.alpha = 0
                cell.currencyImage.alpha = 0
            } else {
                cell.upgradeButton.tag = indexPath.row
                cell.upgradeButton.alpha = 1
                cell.upgradeButton.isEnabled = true
                cell.priceLabel.alpha = 1
                cell.currencyImage.alpha = 1
            }
            cell.currentMultiplierLabel.text = String("x\(ProgressSave.shared.clickMultiplierIndex)")
            cell.price = ProgressSave.shared.clickMultiplierIndex * 10
            cell.priceLabel.text = String("\(cell.price!)")
            
            
            var th = (cell.upgradeTierView.frame.size.width / cell.backView.frame.size.width) / 10
            th = th * CGFloat(ProgressSave.shared.clickMultiplierIndex - 1)
            cell.currentMultiplierLabel.frame.origin.x = (cell.backView.frame.size.width * th) + 6
        case 1:
            cell.buildingName.text = Buildings.shared.buildings[indexPath.row].name
            cell.setViewForIndex(index: ProgressSave.shared.buildingsMultiplierIndex[indexPath.row])
            if ProgressSave.shared.buildingsMultiplierIndex[indexPath.row] >= 10
            {
                cell.upgradeButton.alpha = 0
                cell.upgradeButton.isEnabled = false
                cell.priceLabel.alpha = 0
                cell.currencyImage.alpha = 0
            } else {
                cell.upgradeButton.tag = indexPath.row + 1
                cell.upgradeButton.alpha = 1
                cell.upgradeButton.isEnabled = true
                cell.priceLabel.alpha = 1
                cell.currencyImage.alpha = 1

            }
            cell.currentMultiplierLabel.text = String("x\(ProgressSave.shared.buildingsMultiplierIndex[indexPath.row])")
            cell.price = ProgressSave.shared.buildingsMultiplierIndex[indexPath.row] * 10
            cell.priceLabel.text = String("\(cell.price!)")
            
            var th = (cell.upgradeTierView.frame.size.width / cell.backView.frame.size.width) / 10
            th = th * CGFloat(ProgressSave.shared.buildingsMultiplierIndex[indexPath.row] - 1)
            cell.currentMultiplierLabel.frame.origin.x = (cell.backView.frame.size.width * th) + 6
        default:
            break
        }
        
        cell.backView.layer.cornerRadius = cell.frame.size.height * 0.05
        cell.backView.backgroundColor = Factory.shared.cellColor
        
        
        return cell
    }
    
    
}

extension UpgradesViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {

    }
    
}

class UpgradeViewCell: UITableViewCell
{
    @IBOutlet var buildingName: UILabel!
    
    @IBOutlet var currencyImage: UIImageView!
    @IBOutlet var backView: UIView!
    @IBOutlet var upgradeButton: UIButton!
    
    var price: Int!
    

    
    @IBAction func upgrade(_ sender: UIButton)
    {
        let button = sender
        if ProgressSave.shared.specialEnergy >= price
        {
            ProgressSave.shared.specialEnergy -= BigInt(price!)
            
            if button.tag == 0
            {
                // Click
                ProgressSave.shared.clickMultiplierIndex += 1
                currentMultiplierLabel.text = String("x\(ProgressSave.shared.clickMultiplierIndex)")
                setViewForIndex(index: ProgressSave.shared.clickMultiplierIndex)
                updatePriceForIndex(index: ProgressSave.shared.clickMultiplierIndex)
                animateMultiplierForIndex(index: ProgressSave.shared.clickMultiplierIndex)
            } else {
                // Building
                let buildingIndex = button.tag - 1
                
                ProgressSave.shared.power -= BigInt(BigInt(ProgressSave.shared.buildingsMultiplierIndex[buildingIndex]) * ProgressSave.shared.numberOwned[buildingIndex] * Buildings.shared.buildings[buildingIndex].power)
                ProgressSave.shared.buildingsMultiplierIndex[buildingIndex] += 1
                ProgressSave.shared.power += BigInt(BigInt(ProgressSave.shared.buildingsMultiplierIndex[buildingIndex]) * ProgressSave.shared.numberOwned[buildingIndex] * Buildings.shared.buildings[buildingIndex].power)
                
                
                currentMultiplierLabel.text = String("x\(ProgressSave.shared.buildingsMultiplierIndex[buildingIndex])")
                
                
                
                setViewForIndex(index: ProgressSave.shared.buildingsMultiplierIndex[buildingIndex])
                updatePriceForIndex(index: ProgressSave.shared.buildingsMultiplierIndex[buildingIndex])
                animateMultiplierForIndex(index: ProgressSave.shared.buildingsMultiplierIndex[buildingIndex])
            }
            
            
            let tv = self.superview as! UITableView
            let tvd = tv.delegate as! UpgradesViewController
            tvd.updateBalanceLabel()
        }
    }
    
    @IBOutlet var currentMultiplierLabel: UILabel!
    @IBOutlet var upgradeTierView: UIStackView!
    @IBOutlet var priceLabel: UILabel!
    
    func animateMultiplierForIndex(index: Int)
    {
        var th = (upgradeTierView.frame.size.width / backView.frame.size.width) / 10
        th = th * CGFloat(index - 1)
        let point = CGPoint(x: (backView.frame.size.width * th) + 6,
                            y: currentMultiplierLabel.frame.origin.y)
        UIView.animate(withDuration: 0.1) {
            self.currentMultiplierLabel.frame.origin = point
            if index >= 10
            {
                self.currencyImage.alpha = 0
                self.priceLabel.alpha = 0
            }
        }
    }
    

    
    func setViewForIndex(index: Int)
    {
        for i in 0..<index
        {
            print(upgradeTierView.arrangedSubviews.enumerated())
            if let image = upgradeTierView.arrangedSubviews[i] as? UIImageView
            {
                image.image = UIImage(named: "upgradesclosed.png")
            }
        }
        for i in index..<10
        {
            
            if let image = upgradeTierView.arrangedSubviews[i] as? UIImageView
            {
                image.image = UIImage(named: "upgradesopen.png")
            }
        }
        
        if index >= 10
        {
            upgradeButton.isEnabled = false
            upgradeButton.alpha = 0
            priceLabel.alpha = 0
        }
    }
    
    func updatePriceForIndex(index: Int)
    {
        price = index * 10
        priceLabel.text = String("\(price!)")
    }
    
    //override func
    
}
