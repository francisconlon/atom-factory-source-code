//
//  BackgroundNodeOverlay.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 20/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import SpriteKit

class BackgroundNodeOverlay: SKNode
{
    var backgroundSprite: SKSpriteNode!
    
    
    override init()
    {
        super.init()
        
        backgroundSprite = SKSpriteNode(texture: SKTexture(imageNamed: "icon-18.png"))
        addChild(backgroundSprite)
        backgroundSprite.zPosition = -1
        let grow = SKAction.scale(to: 1.5, duration: 3)
        let shrink = SKAction.scale(to: 1, duration: 3)
        grow.timingMode = .easeInEaseOut
        shrink.timingMode = .easeInEaseOut

        backgroundSprite.run(SKAction.repeatForever(SKAction.sequence([grow,shrink])))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
