//
//  Prefixes.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 08/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation

enum Prefixes: Int
{
    case k = 0
    case M
    case G
    case T
    case P
    case E
    case Z
    case Y
}
