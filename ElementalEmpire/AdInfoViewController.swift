//
//  AdInfoViewController.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 29/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import PersonalizedAdConsent

class AdInfoViewController: UIViewController {

    @IBOutlet var contentView: UIView!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var privacyPolicyScrollView: UIScrollView!
    
    @IBOutlet var labelOne: UILabel!
    @IBOutlet var labelTwo: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //doneButton.backgroundColor = UIColor(red:0.44, green:0.00, blue:0.60, alpha:1.0)
        doneButton.layer.cornerRadius = 5
        contentView.layer.cornerRadius = 5
        
        labelOne.adjustsFontSizeToFitWidth = true
        labelTwo.adjustsFontSizeToFitWidth = true
        labelOne.font = UIFont(name: "SulphurPoint-Regular", size: 1200)
        labelTwo.font = UIFont(name: "SulphurPoint-Regular", size: 1200)
                
        if let advertisers = PACConsentInformation.sharedInstance.adProviders
        {
            var positionY = 0
            let width = Int(privacyPolicyScrollView.frame.size.width)
            let height = Int(privacyPolicyScrollView.frame.size.height / 4)
            for advertiser in advertisers
            {
                let label = UILabel(frame: CGRect(x: 0,
                                                  y: positionY,
                                                  width: Int(Double(width) * 0.9),
                                                  height: height))
                label.numberOfLines = 0
                label.text = String("\(advertiser.name)\n\(advertiser.privacyPolicyURL)")
                label.adjustsFontSizeToFitWidth = true
                label.textAlignment = .center
                label.font = UIFont(name: "SulphurPoint-Regular", size: 1200)
                privacyPolicyScrollView.addSubview(label)
                positionY += Int(label.frame.size.height)
            }
            
            privacyPolicyScrollView.contentSize = CGSize(width: Int(Double(width) * 0.9),
                                                   height: positionY + height)
        }
    }
    
    @IBAction func doneButtonPressed(_ sender: UIButton)
    {
        Factory.shared.animateUIButton(button: sender)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + Factory.shared.duration) {
            self.dismiss(animated: true, completion: nil)
        }
        
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
