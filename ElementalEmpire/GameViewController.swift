//
//  GameViewController.swift
//  ElementalEmpire
//
//  Created by Francis Conlon on 05/03/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import BigInt
import PersonalizedAdConsent

class GameViewController: UIViewController {

    @IBOutlet var progressView: UIProgressView!
    
    @IBOutlet var scoreView: UIView!
    @IBOutlet var storeTableView: UITableView!
    
    @IBOutlet var shopView: UIView!
    @IBOutlet var energyLabel: UILabel!
    @IBOutlet var powerLabel: UILabel!
    var progress: Progress!
    var timerUpdateLabel: Timer!
    var timer: Timer!
    var gameScene: GameScene!
    
    @IBOutlet var oneWatt: UIButton!
    
    
    var progressSave = ProgressSave.shared
    
    @IBAction func buttonPressed(_ sender: UIButton)
    {
        Factory.shared.animateUIButton(button: sender)
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + Factory.shared.duration) {
            self.performSegue(withIdentifier: "upgradeSegue", sender: self)
        }
    }
    
    
    @IBAction func buildingPressed(_ sender: UIButton)
    {
        Factory.shared.animateUIButton(button: sender)
        SKTAudio.sharedInstance().playSoundEffect(Factory.shared.buttonPressSound)

        
        DispatchQueue.main.asyncAfter(deadline: .now() + Factory.shared.duration) {
            self.performSegue(withIdentifier: "buildingSegue", sender: self)
        }
    }
    
    @objc func updateEnergy()
    {
        progressSave.energy += (progressSave.power)
    }
    
    @objc func updateProgressLabel()
    {
        let a = progressSave.energy * 10000
        let b = progressSave.target * 10
        var c = a.quotientAndRemainder(dividingBy: b).quotient / 2
        
        var int = Int64()
        if c > Int64.max
        {
            int = Int64.max
        } else {
            int = Int64(c)
        }
       
        progress.completedUnitCount = int
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        SKTAudio.sharedInstance().playBackgroundMusic("backgroundmusic.mp3")
        
        PACConsentInformation.sharedInstance.requestConsentInfoUpdate(forPublisherIdentifiers: ["pub-8001969410865481"])
        {(_ error: Error?) -> Void in
            if let error = error {
                // Consent info update failed.
            } else {
                // Consent info update succeeded. The shared PACConsentInformation
                // instance has been updated.
            }
        }
        
        elementInfoView.alpha = 0
        elementNameLabel.adjustsFontSizeToFitWidth = true
        elementSymbolLabel.adjustsFontSizeToFitWidth = true
        centerConstraint.constant = self.view.frame.size.width
        elementInfoView.layoutIfNeeded()
        view.layoutIfNeeded()
        
        
        progress = Progress(totalUnitCount: Int64(500))
        progress.completedUnitCount = 0
        progressView.observedProgress = progress
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = view.frame.size.height * 0.03
        
        progressSave.energyLabel = energyLabel
        progressSave.powerLabel = powerLabel
        
        progressSave.updatePowerLabel()
        progressSave.updateEnergyLabel()
        
        self.elementInfoView.frame.origin.y = self.view.frame.size.height * 0.2
        
        timer = Timer.scheduledTimer(timeInterval: 1,
                                     target: self,
                                     selector: #selector(updateEnergy),
                                     userInfo: nil,
                                     repeats: true)
        timerUpdateLabel = Timer.scheduledTimer(timeInterval: 0.1,
                                                target: self,
                                                selector: #selector(updateProgressLabel),
                                                userInfo: nil,
                                                repeats: true)
        
        // Load 'GameScene.sks' as a GKScene. This provides gameplay related content
        // including entities and graphs.
        if let scene = GKScene(fileNamed: "GameScene") {
            
            // Get the SKScene from the loaded GKScene
            if let sceneNode = scene.rootNode as! GameScene? {
                gameScene = sceneNode
                gameScene.progressUpdater = progress
                progressSave.gameScene = gameScene
                // Copy gameplay related content over to the scene
                
                // Set the scale mode to scale to fit the window
                gameScene.scaleMode = .aspectFill
                
                // Present the scene
                if let view = self.view as! SKView? {
                    view.presentScene(gameScene)
                    
                    view.ignoresSiblingOrder = true
                    
                    //view.showsFPS = true
                    //view.showsNodeCount = true
                    //view.showsPhysics = true
                }
            }
        }
    }

    override func viewDidAppear(_ animated: Bool)
    {

    }
    
    func offlineEnergy()
    {
        if (NSDate().timeIntervalSinceReferenceDate > progressSave.lastDateClosed.timeIntervalSinceReferenceDate) && progressSave.power > 0
        {
            self.performSegue(withIdentifier: "offlineEnergy", sender: self)
        }
        print("appear")
    }
    
    func resetGame()
    {
        print("resetting")
        
        ProgressSave.shared.reset()
        gameScene.reset()
        
    }
    
   // override func did
    
    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    func giveOffline()
    {
        UIView.animate(withDuration: 0.2) {
            self.energyLabel.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            UIView.animate(withDuration: 0.15) {
                self.energyLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
        
        gameScene.giveOffline()
    }
    
    func updateForSmallQuantity()
    {
        UIView.animate(withDuration: 0.1) {
            self.energyLabel.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.1) {
                self.energyLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
        UIView.animate(withDuration: 0.1) {
            self.powerLabel.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.1) {
                self.powerLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
    }
    
    func updateForMediumQuantity()
    {
        UIView.animate(withDuration: 0.1) {
            self.energyLabel.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.1) {
                self.energyLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
        UIView.animate(withDuration: 0.1) {
            self.powerLabel.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.1) {
                self.powerLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
    }
    
    func updateForLargeQuantity()
    {
        UIView.animate(withDuration: 0.1) {
            self.energyLabel.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.1) {
                self.energyLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
        UIView.animate(withDuration: 0.1) {
            self.powerLabel.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.1) {
                self.powerLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // Element info view
    
    @IBOutlet var elementInfoView: UIView!
    @IBOutlet var elementSymbolLabel: UILabel!
    @IBOutlet var elementNameLabel: UILabel!
    
    @IBOutlet var centerConstraint: NSLayoutConstraint!
    var displaying = false
    
    func setupInfoViewForElement()
    {
 
        let element = Elements.shared.elements[progressSave.currentElement]
        elementNameLabel.text = String("\(element.name)")
        elementSymbolLabel.text = String("\(element.symbol)")
        centerConstraint.constant = view.frame.size.width
        elementInfoView.layoutIfNeeded()
    }
    
    func appear()
    {
        self.elementInfoView.alpha = 1
        
        centerConstraint.constant = view.frame.size.width / 4
        
        SKTAudio.sharedInstance().playSoundEffect("woosh.wav")
        
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: {
                        self.elementInfoView.layoutIfNeeded()
                        self.view.layoutIfNeeded()


        }) { (completed) in
            print("completed")
            self.glide()
        }
    }
    
    func glide()
    {
        centerConstraint.constant = -(view.frame.size.width / 4)
        
        UIView.animate(withDuration: 1.25,
                       delay: 0,
                       options: .curveLinear,
                       animations: {
                        self.elementInfoView.layoutIfNeeded()
                        self.view.layoutIfNeeded()
        }) { (completed) in
                        self.disappear()
        }
    }
    
    func disappear()
    {
        centerConstraint.constant = -view.frame.size.width
        
        SKTAudio.sharedInstance().playSoundEffect("woosh.wav")

        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: .curveEaseIn,
                       animations: {
                        self.elementInfoView.layoutIfNeeded()
                        self.view.layoutIfNeeded()
        }){ (completed) in
            
            self.centerConstraint.constant = self.view.frame.size.width
            self.elementInfoView.layoutIfNeeded()
            self.view.layoutIfNeeded()
            //self.displaying = false
        }

    }

    
    func displayElementInfo()
    {
        //if displaying == false
        //{
            displaying = true
            setupInfoViewForElement()

            elementInfoView.alpha = 1
            appear()

        //}
    }
    
}


